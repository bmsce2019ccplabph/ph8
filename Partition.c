#include <stdio.h>

int insize(int * s)
{
    printf("No of elements to be entered : ");
    scanf("%d", s);
}

void inarray(int s, int arr[s])
{
    for(int i=0; i<s; i++)
    {
    scanf("%d", &arr[i]);
    }

}

void partition(int s, int arr[s])
{
    int sp=1, lp=s-1, k=arr[0], t;
    
    while(arr[sp]<=k & sp<s-1)
        sp++;
        
    while(arr[lp]>k)
        lp--;
        
    while(sp<lp)
    {
        t=arr[sp];
    	arr[sp]=arr[lp];
    	arr[lp]=t;

        while(arr[sp]<=k)
            sp++;
        
        while(arr[lp]>k)
            lp--;

    }	
        
    t=arr[0];
    arr[0]=arr[lp];
    arr[lp]=t;

}
void outarray(int s, int arr[s])
{
    for(int i=0; i<s; i++)
    {
    printf("%d\t", arr[i]);
    }
    printf("\n");
}

int main()
{

    int size, n;
    printf("Enter no of arrays:\n");
    scanf("%d",&n);

    insize(&size);
    
    int arr[size];

    for(int i=0; i<n; i++)
	{
		printf("ARRAY NO %d\n", i+1);
    	inarray(size, arr);
    
    	outarray(size,arr);
    
    	partition(size, arr);
    
    	outarray(size, arr);
	}

    return 0;
}


