a)
#include <stdio.h>

float input(float a)
{
    scanf("%f", &a);
    return a;
}

float great(float a, float b, float c)
{ float g;
  if(a>b)
  {
      if(a>c)
          g=a;
      }
      
  if(b>c)
  {
      if(b>a)
          g=b;
      }
  if(c>a)
  {
      if(c>b)
          g=c;
      } 
  return g;
}

void output(float a)
{
    printf("Greatest of the given three numbers is %.2f", a);
}

int main()
{float a, b, c, g;
printf("Enter three numbers.\n");
a=input(a);
b=input(b);
c=input(c);
g=great(a, b, c);
output(g);
return 0;
}


b)#include <stdio.h>

float input(float a)
{
    scanf("%f", &a);
    return a;
}

float great(float a, float b, float c)
{ float g;
  if(a>b && a>c)
    g=a;
  else if(b>c && b>a)
    g=b;
   else if(c>a && c>b)
    g=c;
    
    
  return g;
}

void output(float a)
{
    printf("Greatest of the given three numbers is %.2f", a);
}

int main()
{float a, b, c, g;
printf("Enter three numbers.\n");
a=input(a);
b=input(b);
c=input(c);
g=great(a, b, c);
output(g);
return 0;
}

c)
#include <stdio.h>

float input(float a)
{
    scanf("%f", &a);
    return a;
}

float great(float a, float b, float c)
{ float g;
   g=(a>b && a>c) ? a : (b>c ? b : c);
  return g;
}

void output(float a)
{
    printf("Greatest of the given three numbers is %.2f", a);
}

int main()
{float a, b, c, g;
printf("Enter three numbers.\n");
a=input(a);
b=input(b);
c=input(c);
g=great(a, b, c);
output(g);
return 0;
}


