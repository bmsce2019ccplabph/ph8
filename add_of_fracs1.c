#include <stdio.h>

struct frac
{
    int n;
    int d;
};

typedef struct frac frac;

void input(int n,frac j[n])
{   int i;
    
    for(i=0; i<n; i++)
        {
            printf("Enter a number");
            scanf("%d/%d", &j[i].n, &j[i].d);
        }
}

frac simp(frac f)
{   int t;
    frac s;
    s.n=f.n;
    s.d=f.d;
      
    while(f.d!=0)
    { 
        t=f.d;
        f.d=f.n % f.d;
        f.n=t; 
        
    }
     
    s.n=s.n/f.n;
    s.d=s.d/f.n;
     
    return s;
}


frac add(int n, frac j[n])
{   frac s;
    int i;
    s.n=((j[0].n)*(j[1].d))+((j[1].n)*(j[0].d));
    s.d=(j[0].d)*(j[1].d);
  
    
    for(i=2; i<n; i++)
        {
            s.n=((s.n)*(j[i].d))+((s.d)*(j[i].n));
            s.d=(s.d)*(j[i].d);
        }
    return s;
}

void output(frac s)
{
    printf("The sum of given numbers is %d/%d\n", s.n, s.d);
}

int main()
{   int n;
    printf("How many fractions do you want to add");
    scanf("%d", &n);
    frac num[n];
    
    input(n, num);
    frac s=add(n, num);
    output(s);
    frac j=simp(s);
    output(j);
    return 0;
}