#include <stdio.h>
#include <stdlib.h>

int insize(int * s)
{
    printf("No of elements to be entered : ");
    scanf("%d", s);
}

void inarray(int s, int arr[s])
{
    for(int i=0; i<s; i++)
    	scanf("%d", &arr[i]);
}

int compare(const void *p1, const void *p2)
{	
	return (*(int *)p1)-(*(int *)p2);
}

void outarray(int s, int arr[s])
{
    for(int i=0; i<s; i++)
    {
    printf("%d\t", arr[i]);
    }
    printf("\n");
}

int main()
{
	int s;
	insize(&s);
	
	int a[s];
	inarray(s, a);
	
	qsort(&a[0], s, sizeof(int), compare);
	
	outarray(s, a);
	
	return 0; 
}