#include <stdio.h>
int input(int n)
{
    scanf("%d", &n);
    return n;
}

int sos(int n)
{
    int s;
    for(int i=1; i<=n; i++)
        if(i%2==0)
            s=s+(i*i);
        else
            s=s+0;
    
    return s;
}
void output(int n, int s)
{
   printf("Sum of squares of even numbers under %d=%d\n", n, s);
    }

int main()
{   
    int n, s;
    printf("Enter a number.");
    n=input(n);
    
    s=sos(n);
    
    output(n, s);
    
    return 0;
}