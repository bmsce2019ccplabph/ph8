#include <stdio.h>

void input(int n,int j[n])
{   int i;
    
    for(i=0; i<n; i++)
        {
            printf("Enter a number");
            scanf("%d", &j[i]);
        }
}

int add(int n,int j[n])
{   
    int s=0, i;
    for(i=0; i<n; i++)
        {
            s=s+j[i];
        
        }
    return s;
}

void output(int s)
{
    printf("The sum of given numbers is %d", s);
}

int main()
{    int n;
    printf("How many numbers do you want to add");
    scanf("%d", &n);
    int num[n];
    
    input(num, n);
    int s=add(num, n);
    output(s);
    return 0;
}