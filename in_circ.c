#include <stdio.h>
#include <math.h>
struct point
{
    float x;
    float y;
};

struct circle
{
    struct point a;
    float r;
};

typedef struct circle cir;
typedef struct point poi;

cir incir()
{   
    cir c;
    printf("Enter the radius");
    scanf("%f", &c.r);
    printf("Enter the centre of your circle");
    scanf("%f, %f", &c.a.x, &c.a.y);
    return c;
}

poi inpoi()
{   poi a;
    printf("Enter a coordinate.");
    scanf("%f, %f", &a.x, &a.y);
    return a;
}

int inscir(poi b, cir c)
{   int t;
    float d=sqrt(((b.x-c.a.x)*(b.x-c.a.x))+((b.y-c.a.y)*(b.y-c.a.y)));
    if(d<c.r)
        t=1;
    else if(d==c.r)
        t=2;
    else if(d>c.r)
        t=3;
return t;
}

void output(int t)
{
     if(t==1)
        printf("Point lies in circle");
    else if(t==2)
       printf("Point lies on circle");
    else if(t==3)
        printf("Point lies outside circle");
}

int main()
{
   poi a; int t;
   cir c;
   a=inpoi();
   c=incir();
    t=inscir(a, c);
    output(t);
    return 0;
}