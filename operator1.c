#include <stdio.h>

int main()
{
	int a, b, s, p, d, q, m;
    printf("Enter two numbers.");
    scanf("%d %d", &a, &b);
    
    s=a+b;
    d=a-b;
    p=a*b;
    q=a/b;
    m=a%b;
    
    printf("The sum is %d.\nThe difference is %d.\nThe product is %d.\nThe quotient is %d.\nThe remainder is %d.\n", s, d, p, q, m);
    
    return 0;
    
}
