#include <stdio.h>

int input()
{
    int m;
    printf("Enter a number, enter -1 if you don't wish to enter more numbers.");
    scanf("%d", &m);
    return m;
}

int compute()
{
    
    int i;
    int m=input();
    int n=input();
    while(n!=(-1))
    {
        
        if(m==n && n!=-1)
            i=0;
        else if(m!=n && n!=(-1))
            i=1;
        n=input();
        
    }
    return i;
}

void output(int a)
{
    printf("%d", a);
}

int main()
{
    int m=compute();
    output(m);
    return 0;
}


