#include <stdio.h>
#include <math.h>
int main()
{   float a, b, c, r1, r2, d;
    printf("Enter coeffecients of quadratic equation");
    scanf("%f %f %f", &a, &b, &c);
    d=(b*b)-(4*a*c);
      
        
    if(a!=0)
    {
          if(d>0)
            printf("Real and unequal roots\n");
          else if(d==0)
            printf("Real and equal roots\n");
          else if(d<0)
            printf("Imaginary roots");
            
        r1=(-b+sqrt(d))/2*a;
        r2=(-b-sqrt(d))/2*a;
         
        printf("%f %f", r1, r2);
    }
    
    else 
        printf("Coeffecient of x^2 cannot be zero");
   
  return 0;
}