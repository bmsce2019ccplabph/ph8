#include <stdio.h>
int input()
{
    int a;
    printf("Enter a number");
    scanf("%d", &a);
    return a;
}

int factorial(int a)
{
    int p=1, i=1;
    for(;i<=a; i++)
    {
        p=p*i;
    }
    
    return p;
}

void output(int a)
{
    printf("Factorial=%d\n", a);
}


int main()
{
    int n=input();
    int f=factorial(n);
    output(f);
    return 0;
}