#include <stdio.h>

float input(float a)
{
    scanf("%f", &a);
    return a;
}

float cel(float f)
{
    float c=(f-32)*5/9;
    
    return c;
}

void output(float a)
{
    printf("Temperature in degree celcius is %f", a);
}

int main()
{float c, f;
printf("Enter temperature in fahreheit.\n");
f=input(c);
c=cel(f);
output(c);
return 0;
}
