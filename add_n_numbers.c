#include <stdio.h>

int input()
{   
   int c;
   printf("Enter a number.");
   scanf("%d", &c);
   return c;
}
    
int sum(int*a, int*b)
{   int s=0;
    s=*a+*b;
    int d;
    char c;
    printf("Do you want to continue?");
    scanf(" %c", &c);
    while(c=='y')
    {
        printf("Enter a new  number.");
        scanf("%d", &d);
        s=s+d;
       
        printf("Do you want to continue?");
        scanf(" %c", &c);
    } 
    return s;
}
    
void output(int a)
{
    printf("Sum of entered numbers is %d",a);
}

int main()
{
    int a, b, s;
    a=input();
    b=input();
    
    s=sum(&a, &b);
    
    output(s);
    return 0;
}
