#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void insize( int  *n){
	printf("No of strings to be entered : ");
    	scanf("%d", n);
}

void instrings(int n, char *s[n]){
	char st[20];
	int l;
	for(int i=0; i<n; i++){
		printf("Enter string : ");
		scanf("%s", st);
		l= strlen(st);
		char *p = (char*)malloc( l * sizeof(char));
		strcpy(p, st);
		s[i] = p;
	}
}
int compare(const void *p1, const void *p2)
{
	return strcmp( *(const char **)p1, *(const char **)p2);
}

void outstrings(int n, char *s[n])
{
    for (int i=0; i<n; i++)
    {
        puts(s[i]);
    }
}

int main(){
	int n;
	insize(&n);
	printf("%d", n);

	char *s[n];
    	instrings(n, s);
	qsort(s, n, sizeof(char *), compare);
	outstrings(n, s);
}
