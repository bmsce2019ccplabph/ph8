#include <stdio.h>

int size()
{
    int a;
    printf("Enter size of array");
    scanf("%d", &a);
    return a;
}

void inarray(int n, int a[n])
{
    for(int i=0; i<n; i++)
    {
        printf("Enter %dth element", i);
        scanf("%d", &a[i]);
    }
}

int distinct(int n, int a[n])
{
    int k=0;
    for(int i=0; i<n; i++)
      { 
          for(int j=0; j<n; j++) 
          { 
              if(a[i]==a[j] && i!=j)
                k++;
          }
      }
      if(n<(k/2))
          return 1;
       else    
     return n-(k/2);
}

void output(int a)
{
        printf("%d", a);
}

int main()
{
    int n=size();
    int a[n];
    inarray(n, a);
    
    int s=distinct(n, a);
    output(s);
    return 0;
    
}
