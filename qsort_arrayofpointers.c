#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int insize(int * n)
{
    printf("No of strings to be entered : ");
    scanf("%d", n);
}

void instrings(int n, char *s[n])
{
    for (int i=0; i<n; i++)
    {
       scanf("%s", s[i]);
    }
}
int compare(const void *p1, const void *p2)
{
	return strcmp( (const char*) p1, (const char*)p2);
} 

void outstrings(int n, char *s[n])
{
    for (int i=0; i<n; i++)
    {
        printf("%s", s[i]);
    }
}

int main()
{
    int n;
    insize(&n);
    char *s[20];
    instrings(n, s);
	qsort(s[0], n, 20*sizeof(char), compare);

    outstrings(n, s);
    
    return 0;
}

