#include <stdio.h>
#include <math.h>
float distance (int x1, int y1, int x2, int y2)
{
    int a, b;
    float e;
    
    a=pow((x2-x1),2);
    b=pow((y2-y1),2);
    e=sqrt(a+b);
    return e;
}
    
int main()
{
    int x, y, z, p;
    float d;
    printf("Enter two coordinates");
    scanf("%d %d %d %d", &x, &y, &z, &p);
    d=distance(x, y, z, p);
    
    printf("distance=%f", d);
    return 0;
}
    