#include <stdio.h>
# define pi 3.14
float input(int n)
{
    scanf("%f", &n);
    return n;
}

float cir(float r)
{  
float cr=pi*2*r;
return cr;
}


void output(float n)
{
    printf("Circumference=%.2f\n", n);
}

int main()
{   
  float r, a;
  printf("Enter radius of circle.\n");
  r=input(r);
  a=cir(r);
  output(a);
  return 0;
}