#include <stdio.h>

struct results
{
    int b[20];
};

int input()
{   
    int a;
    printf("Enter a number");
    scanf("%d", &a);
    return a;
}
struct results binary(int a)
{   struct results r;
    for(int i=0; a!=0 ; i++)
    {
        r.b[i]=a%2;
        a=a/2;
    }
        
    return r;
}

struct results octal(int a)
{   struct results r;
    for(int i=0; a>0; i++)
    {
        r.b[i]=a%8;
        a=a/8;
    }
    
    return r;
}

struct results hex(int a)
{   struct results r;
    for(int i=0; a>0; i++)
    {
        r.b[i]=a%16;
        a=a/16;
    }
    
    return r;
}


void revoutput(struct results a)
{
    for(int i=19; i>=0; i--)
    {
        printf("%d", a.b[i]);
    }
}

int main()
{
    int a;
    static struct results rb, ro, rh;

    a=input();
    rb=binary(a);
    ro=octal(a);
    rh=hex(a);
    revoutput(rb);
    printf("\n");
    revoutput(ro);
    printf("\n");
    revoutput(rh);
    return 0;
}