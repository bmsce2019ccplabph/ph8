#include <stdio.h>
int input()
{
    int a;
    printf("Enter a year");
    scanf("%d", &a);
    return a;
}

char leap(int a)
{    char g;
    if(a%100==0)
        {
        if(a%400==0)
             g='l';
         
         else
             g='n';
        }
       
    if(a%100!=0)
        {
        if(a%4==0)
            g='l';
        else 
            g='n';
        }
    
    return g;
}

void output(char g)
{
    if(g=='l')
        printf("It is a leap year");
    else if(g=='n')
        printf("It is not a leap year");
}

int main()
{
    int c=input();
    char g=leap(c);
    output(g);
    return 0;
}

