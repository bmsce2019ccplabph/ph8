#include <stdio.h>

float input(float a)
{
    scanf("%f", &a);
    return a;
}

float great(float a, float b)
{ float g;
  if(a>b)
    g=a;
  else
    g=b;
    
  return g;
}

void output(float a)
{
    printf("Greatest of the given two numbers is %.2f", a);
}

int main()
{float a, b, g;
printf("Enter two numbers.\n");
a=input(a);
b=input(b);
g=great(a, b);
output(g);
return 0;
}
