#include <stdio.h>
#include <math.h>

struct roots{
    
    float d, r1, r2;
};

typedef struct roots ros;

void input(int *a, int *b, int *c)
{
    printf("Enter coeffecients of quadratic equation");
    scanf("%d %d %d", a, b, c);
}

ros roots(int a, int b, int c)
{   ros r;
    r.d=(b*b)-(4*a*c);
    r.r1=(-b+sqrt(r.d))/2*a;
    r.r2=(-b-sqrt(r.d))/2*a;
    
    return r;
}

int nature(int d)
{   int g;
    if(d>0)
        g=1;
    else if(d==0)
        g=2;
    else if(d<0)
        g=3;
    return g;
}

void shownat(int g)
{
     switch(g)
    {
        case 1: printf("Real and unequal\n");
                break;
        case 2: printf("Real and equal\n");
                break;
        case 3: printf("Imaginary\n");
    }
    
}
void showroots(ros r)
{
    if(r.d>0)
    printf("The roots are %f and %f\n", r.r1, r.r2);
    
    else if(r.d==0)
    printf("The root is %f\n", r.r1);
    
    else if(r.d<0)
    printf("The roots can't be displayed.\n");
}

int main()
{   int a, b, c, d;
    ros r;
    input(&a, &b, &c);
    d=(b*b)-(4*a*c);
    int g=nature(d);
    shownat(g);
    r=roots(a, b, c);
    showroots(r);
    return 0;
}