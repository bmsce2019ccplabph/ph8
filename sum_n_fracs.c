#include <stdio.h>

struct frac
{
    int n;
    int d;
};

typedef struct frac frac;

void input(int n,frac j[n])
{   int i;
    
    for(i=0; i<n; i++)
        {
            printf("Enter a number");
            scanf("%d/%d", &j[i].n, &j[i].d);
            
        }
}



frac add(int x, frac j[x])
{  
    frac s;
    int i;
    s.n=((j[0].n)*(j[1].d))+((j[1].n)*(j[0].d));
    s.d=(j[0].d)*(j[1].d);
    for(i=2; i<x; i++)
        {
            s.n=((s.n)*(j[i].d))+((s.d)*(j[i].n));
            s.d=(s.d)*(j[i].d);
        }
    return s;
}

void output(frac s)
{
    printf("The sum of given numbers is %d/%d", s);
}

int main()
{   
    int n;
    printf("How many fractions do you want to add");
    scanf("%d", &n);
    frac num[n];
    
    input(n, num);
    frac s=add(n, num);
    output(s);
    return 0;
}