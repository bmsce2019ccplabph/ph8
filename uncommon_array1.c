#include <stdio.h>

int size()
{
    int a;
    printf("Enter size of array");
    scanf("%d", &a);
    return a;
}

void inarray(int n, int a[n])
{
    for(int i=0; i<n; i++)
    {
        printf("Enter %dth element", i);
        scanf("%d", &a[i]);
    }
}

int uncommon(int n, int a[n], int m, int b[m], int l, int c[l])
{
    int k=0;
    for(int i=0; i<n; i++)
    {
        for(int j=0; j<m; j++)
        {
            if(a[i]==b[j])
                break;
            if(a[i]!=b[j])
                {
                    c[k]=a[i];
                    k++;
                }
        }
    }
    
    if(k>0)
    {
        return 1;
        }
    
}

int smallest(int m, int a[m])
{
    int s=a[0];
    for(int i=0; i<m; i++)
    {
        if(a[i]<s)
            s=a[i];
    }
    return s;
}

void output(int s)
{
    if(s==0)
    printf("The smallest number in array 1 not present in array 2 is %d", s);
    else if(s>0)
        printf("NO");
}

int main()
{
    int n=size();
    int a[n];
    inarray(n, a);
     
    int m=size();
    int b[m];
    inarray(m, b);
    
    int l=m>n ? m : n;
    int c[l];
    
    
    uncommon(n, a, m, b, l, c);
    int s=smallest(l, c);
    output(s);
    return 0;
}