#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct employee{
	int id;
	char name[20];
} emp;

void insize(int *n){
	printf("No of employee records to be entered : ");
    	scanf("%d", n);
}

void inarray(int n, emp e[n]){
	for(int i = 0; i < n; i++){
		printf("Enter id: ");
		scanf("%d", &e[i].id);
		printf("Enter name: ");
		scanf("%s", e[i].name);
	}
}

void outarray(int n, emp e[n]){
	for(int i = 0; i < n; i++)
		printf("Id = %d\tName = %s\n", e[i].id, e[i].name);
	printf("\n");
}

int compareid(const void * p1, const void *p2){	
	return (*(emp *)p1).id - (*(emp *)p2).id;
}

int comparename(const void * p1, const void *p2){	
	return strcmp((*(emp *)p1).name, (*(emp *)p2).name);
}

int main(){
	int n;
	insize(&n);

	emp e[n];
	inarray(n, e);
	qsort(e, n, sizeof(emp), compareid);
	outarray(n, e);	
	qsort(e, n, sizeof(emp), comparename);
	outarray(n, e);

	return 0;
}
