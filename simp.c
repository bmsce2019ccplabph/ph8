#include <stdio.h>


struct fraction
{
    int a;
    int b;
};

struct fraction input(struct fraction x)
{
    printf("Enter a fraction.");
    scanf("%d/%d", &x.a, &x.b);
    return x;
}


struct fraction add(struct fraction x, struct fraction y)
{   
    int f;
    struct fraction c;
    c.a=(x.a)*(y.b)+(x.b)*(y.a);
    c.b=(x.b)*(y.b);
    return c;
}

struct fraction simp(struct fraction y)
{
    int r, g;
    struct fraction x;
    x.a=y.a;
    x.b=y.b;
    if(x.a>x.b)
    {
        while(r!=0)
        {
        r=x.b%x.a;
        x.b=x.b/r;
        }
        g=x.b;
    }
    
    else if(x.b>x.a)
    {
        while(r!=0)
        {
        r=x.a%x.b;
        x.a=x.a/r;
        }
        g=x.a;
    }
    y.a=y.a/g;
    y.b=y.b/g;
    
    return y;
}

void output(struct fraction d)
{
   printf("%d/%d", d.a, d.b);
} 


int main()
{
    struct fraction x, y, p, d;
    x=input(x);
    y=input(y);
    p=add(x, y);
    d=simp(p);
    
    
    return 0;
}
