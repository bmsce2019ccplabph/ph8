#include <stdio.h>
char input()
{
    char a;
    printf("Enter a character");
    scanf(" %c",&a);
    return a;
}

char vowel(char a)
{   char g;
    if(a=='a' || a=='e' || a=='i' || a=='o' || a=='u')
        g='v';
    else
        g='c';
    
    return g;
}

void output(char g)
{
    if(g=='v')
        printf("It is a vowel");
    else if(g=='c')
        printf("It is a constant");
}

int main()
{
    char c=input();
    char g=vowel(c);
    output(g);
    return 0;
}

