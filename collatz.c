#include <stdio.h>

int input()
{
    int a;
    printf("Enter size of array");
    scanf("%d", &a);
    return a;
}

int function(int n)
{
    if (n%2!=0)
        return (3*n)+1;
    else if(n%2==0)
        return n/2;
}

int functimes(int n)
{
    int i=0;
    while(n!=1)
    {
        n=function(n);
        i++;
    }
    return i;
}

void output(int a)
{
        printf("%d", a);
}

int main()
{
    int n=input();
    
    int s=functimes(n);
    output(s);
    return 0;
    
}