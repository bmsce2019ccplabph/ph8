#include <stdio.h>
int input()
{
    int a;
    printf("Enter a number");
    scanf("%d", &a);
    return a;
}

void leap(int *a, int *b)
{
int t=*a;
*a=*b;
*b=t;
}

void output(int a, int b)
{
    printf("a=%d, b=%d", a, b);
}

int main()
{
    int a=input();
    int b=input();
    leap(&a, &b);
    output(a, b);
    return 0;
}
