#include <stdio.h>
int main()
{
    int a, ps=0, ns=0, psn=0, nsn=0;
    
    printf("Enter a number and enter -1 to stop.");
    scanf("%d", &a);
    while(a!=-1)
    {
        if(a>0)
            {
                ps=ps+a;
                psn++;
            }
            
        if(a<0)
            {
                ns=ns+a;
                nsn++;
            }
            
        printf("Enter a number and enter -1 to stop.");
        scanf("%d", &a);
    }
    
    float psa=(float)ps/psn;
    float nsa=(float)ns/nsn;
    
    printf("Sum of positive numbers is %d", ps);
    printf("Sum of negative numbers is %d", ns);
    printf("Avg of positive numbers is %f", psa);
    printf("Avg of negative numbers is %f", nsa);
    
}