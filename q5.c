#include <stdio.h>

float input(float r)
{
    scanf("%f", &r);
    return r;
}

float area(float r)
{
    float a=3.14*r*r;
    
    return a;
}

void output(float a)
{
    printf("Area of cicle is %.2f", a);
}

int main()
{float a, r;
printf("Enter radius of circle.\n");
r=input(r);
a=area(r);
output(a);
return 0;
}
