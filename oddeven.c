#include <stdio.h>
void oe(int a)
{
    if(a%2==0)
        printf("It is even");
    else if(a%2!=0)
        printf("It is odd");
}

int main()
{   int a;
    printf("Enter a number");
    scanf("%d", &a);
    oe(a);
    return 0;
}