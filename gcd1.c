#include <stdio.h>

int input()
{   int a;
    printf("Enter a no");
    scanf("%d", &a); 
    return a;
}

int gcd(int a, int b)
{   int r;


    while (b != 0)
       {r = b; 
       b = a% b; 
       a = r; 
       }
       
    return a;
    
}

void output(int a)
{
    printf("Greatest common divisor=%d", a );
}

int main()
{
    int a, b, f;
    a=input();
    b=input();
    f=gcd(a, b);
    
    output(f);
    return 0;
}

    