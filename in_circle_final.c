#include <stdio.h>
#include <math.h>

struct point
{
    float x;
    float y;
};

struct circle
{
    struct point a;
    float r;
};
typedef struct circle cir;
typedef struct point poi;

struct result
{
    poi d;
    cir s[100];
};

struct results
{
    int n;
   struct result c[100];
};



void inpcirs(int n, cir c[n])
{   
    for (int i=0; i<n; i++)
    {printf("Enter the radius");
    scanf("%f", &c[i].r);
    printf("Enter the centre of your circle");
    scanf("%f, %f", &c[i].a.x, &c[i].a.y);
    }
}

void inpois(int n, poi p[n])
{   
    for (int i=0; i<n; i++)
   
    {
    printf("Enter a coordinate.");
    
    scanf("%f, %f", &p[i].x, &p[i].y);
    
    }
    
}

struct results inscir(int np, poi b[np], int nc, cir c[nc])
{  struct results g;
    int k=0;
    for(int i=0; i<np; i++)
    {
        for(int j=0; j<nc; j++)
        {
            float d=sqrt(((c[j].a.x-b[i].x)*(c[j].a.x-b[i].x))+((c[j].a.y-b[i].y)*(c[j].a.y-b[i].y)));
            if(d<c[j].r)
            {   
                  
                        g.c[k].d.x=b[i].x;
                        g.c[k].d.y=b[i].y;
                        g.c[k].s[k].a.x=c[j].a.x;
                        g.c[k].s[k].a.y=c[j].a.y;
                        g.c[k].s[k].r=c[j].r;
                        k++;
                  
            }
        }
    } 
    g.n=(k); 
    printf("%d\n", k);
    return g;

}

void output(struct results a)
{   
    for(int i=0; i<(a.n); i++)
    {
        printf("The point (%f, %f) lies inside circle of radius %f and centre (%f, %f).\n", a.c[i].d.x, a.c[i].d.y, a.c[i].s[i].r, a.c[i].s[i].a.x, a.c[i].s[i].a.y);
        
    }
}

int main()
{ 
    int np, nc;
    struct results r;
    printf("How many circles do you want to enter?");
    scanf("%d", &nc);
    cir c[nc];
    inpcirs(nc, c);
    
    printf("How many points do you want to enter?");
    scanf("%d", &np);
    poi p[np];
    inpois(np, p); 

    r=inscir(np, p, nc, c);
    
    output(r);
    
    
    
    return 0;
    
}

