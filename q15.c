#include <stdio.h>
int input(int n)
{
    scanf("%d", &n);
    return n;
}

int fact(int n)
{
int f=1;
if(n==0)
    f=1;
else if(n>0)
    for(int i=1; i<=n; i++)
        f=f*i;

return f;
}

void output(int n)
{
   printf("Factorial=%d\n", n); 
}
int main()
{   
    int n, f;
    printf("Enter a number.");
    n=input(n);
    f=fact(n);
    
    output(f);
    return 0;
}