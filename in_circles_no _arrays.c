#include <stdio.h>
#include <math.h>
struct point
{
    float x;
    float y;
};

struct circle
{
    struct point a;
    float r;
};

typedef struct circle cir;
typedef struct point poi;

void inpcirs(int n, cir c[n])
{   
    for (int i=0; i<n; i++)
    {printf("Enter the radius");
    scanf("%f", &c[i].r);
    printf("Enter the centre of your circle");
    scanf("%f, %f", &c[i].a.x, &c[i].a.y);
    }
}

void inpois(int n, poi p[n])
{   
    for (int i=0; i<n; i++)
   
    {
    printf("Enter a coordinate.");
    scanf("%f, %f", &p[i].x, &p[i].y);
    }
    
}

void inscir(int np, poi b[np], int nc, cir c[nc])
{  cir in[nc];
    for(int i=0; i<np; i++)
    {
        for(int j=0; j<nc; j++)
        {
            float d=sqrt(((c[j].a.x-b[i].x)*(c[j].a.x-b[i].x))+((c[j].a.y-b[i].y)*(c[j].a.y-b[i].y)));
            if(d<c[j].r)
            {   
               printf("Point (%.2f, %.2f) lies in circle of radius %.2f and center (%.2f, %.2f).\n", b[i].x, b[i].y, c[j].r,c[j].a.x, c[j].a.y);
            }
        }
    } 

}




int main()
{ 
    int np, nc;
    printf("How many circles do you want to enter?");
    scanf("%d", &nc);
    cir c[nc];
    inpcirs(nc, c);
    
    printf("How many points do you want to enter?");
    scanf("%d", &np);
    poi p[np];
    inpois(np, p); 

    inscir(np, p, nc, c);
    
    
    
    return 0;
    
}

